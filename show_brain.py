#!/usr/bin/env python3

from pathlib import Path
from typing import Union

import defopt
import numpy as np
import imageio
import dask
import dask.array as da
from dask.cache import Cache
import xarray as xr
import holoviews as hv
import holoviews.operation.datashader as hd
import datashader.transfer_functions as tf
import datashader as ds
import panel as pn


def load_stack(tiff_folder: Union[Path, str]) -> xr.DataArray:
    """load a folder of .tif files into a xarray"""

    tiff_files = sorted(Path(tiff_folder).glob("*.tif"))

    img0 = imageio.imread(tiff_files[0])
    imgs = [dask.delayed(imageio.imread)(fname) for fname in tiff_files]
    stack = [da.from_delayed(img, shape=img0.shape, dtype=img0.dtype) for img in imgs]
    stack = da.stack(stack)

    dset = xr.DataArray(
        stack[:, ::-1, :],
        dims=["frame", "y", "x"],
        coords={
            "frame": np.arange(stack.shape[0]),
            "y": np.arange(stack.shape[1]),
            "x": np.arange(stack.shape[2]),
        },
    )
    return dset


CMAPS = {
    "green": (0, 255, 0),
    "red": (255, 0, 0),
    "blue": (0, 0, 255),
    "gray": (255, 255, 255),
    "off": (0, 0, 0),
}


def build_viewer(stack):
    """assemble the core elements of the viewer"""

    nchans, nframes, ny, nx = stack.shape

    widgets = {}
    widgets["frame"] = pn.widgets.IntSlider(name="frame", start=0, end=nframes - 1)

    cmap_names = list(CMAPS)
    for i in range(nchans):
        widgets[f"clim_{i}"] = pn.widgets.IntRangeSlider(
            name="min/max", value=(0, 900), start=0, end=1500
        )
        widgets[f"threshold_{i}"] = pn.widgets.IntSlider(
            name="threshold", value=0, start=0, end=1500
        )
        widgets[f"cmap_{i}"] = pn.widgets.Select(
            name="colormap", options=cmap_names, value=cmap_names[i],
        )

    # inpired by https://github.com/holoviz/holoviews/issues/1795#issuecomment-320920793
    @pn.depends(**widgets)
    def plot_stack(x_range, y_range, frame, **kwargs):
        canvas = ds.Canvas(x_range=x_range, y_range=y_range)

        imgs_shaded = []
        for i in range(nchans):
            img = stack[i, frame]
            img = img.where(img > kwargs[f"threshold_{i}"], 0)
            img_raster = canvas.raster(img.compute(), interpolate="nearest")
            img_shaded = tf.shade(
                img_raster,
                CMAPS[kwargs[f"cmap_{i}"]],
                span=kwargs[f"clim_{i}"],
                how="linear",
                min_alpha=0,
            )
            imgs_shaded.append(img_shaded)

        img_rgb = tf.stack(*imgs_shaded, how="add")
        img_rgb = tf.set_background(img_rgb, "black")

        bounds = (x_range[0], y_range[0], x_range[1], y_range[1])
        hv_rgb = hv.RGB(hd.shade.uint32_to_uint8_xr(img_rgb), bounds=bounds)
        hv_rgb = hv_rgb.relabel(f"frame: {frame}, dims: {stack.shape}")

        return hv_rgb

    xy_stream = hv.streams.RangeXY(x_range=(0, nx - 1), y_range=(0, ny - 1))

    return widgets, plot_stack, xy_stream


def build_webapp(channels_folders):
    """load data and assemble the full webapp"""

    stack = xr.concat([load_stack(chan) for chan in channels_folders], dim="chan")
    widgets, plot_stack, xy_stream = build_viewer(stack)
    hv_img = hv.DynamicMap(plot_stack, streams=[xy_stream])

    # adjust plot and widgets style
    hv_img.opts(frame_width=1000, aspect="equal")

    for widget in widgets.values():
        widget.margin = 10

    # create tabs for each channel
    channel_widgets = [
        (
            f"channel {i}",
            pn.Column(
                widgets[f"cmap_{i}"], widgets[f"clim_{i}"], widgets[f"threshold_{i}"]
            ),
        )
        for i in range(stack.shape[0])
    ]
    channel_tabs = pn.Tabs(*channel_widgets)

    # create the page layout
    pane = pn.Row(hv_img, pn.Column(channel_tabs, widgets["frame"]))
    return pane


def main(
    *channels_folders: Path, port: int = 0, open: bool = True, cache_size: float = 1.0
):
    """Display large brain slices in a web browser

    :param channels_folders: folder of .tif files for each channel
    :param port: port number for the webserver
    :param open: open a browser tab
    :param cache_size: cache for dask in GB
    """

    cache = Cache(cache_size * 1e9)
    cache.register()
    pane = build_webapp(channels_folders)
    pane.show(port=port, open=open, verbose=True)


if __name__ == "__main__":
    defopt.run(main)
