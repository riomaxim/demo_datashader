from pathlib import Path

from dask.cache import Cache

from show_brain import build_webapp

# register 1GB of cache for dask
cache = Cache(1e9)
cache.register()

projects_folder = Path("/mnt/rioma/winstor/swc/mrsic_flogel/public/projects")
stack_folder = (
    projects_folder / "YuHa_20140614_singleCellTracing/YH283_160901/stitchedImages_100/"
)
chan_folders = [stack_folder / "1", stack_folder / "2"]

pane = build_webapp(chan_folders)
pane.servable()
