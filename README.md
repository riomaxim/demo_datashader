# Datashader demo

This repository presents a small demo of web-based display for large brain stacks.


## Installation

Create a Python virtual environment (with conda for example) and install the
dependencies listed in the `requirements.txt` file:
```
conda create -p venv python=3.8
conda activate ./venv
pip install -r requirements-pinned.txt
```


## Local application

You can use the `show_brain.py` script to start a local server to explore some
brains:
```
python show_brain.py <channel 1 folder> <channel 2 folder>
```

Use the `-h` flag to get more information about the options (port number, etc.).


## Demo webapp

Start a webserver using `panel` and the name of the demo:
```
panel serve show_brain_demo.py
```

You can specify the port number and allow others to connect to your machine by
passing your machine address explicitly (here `gluegun`):
```
panel serve --port 9000 --allow-websocket-origin gluegun.mrsic-flogel.swc.ucl.ac.uk:9000 show_brain_demo.py
```

Note that for the demo, stack paths are hardcoded so feel free to adapt it for
your data ;-).
